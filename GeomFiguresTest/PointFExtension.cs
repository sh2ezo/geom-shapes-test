﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace GeomFiguresTest
{
    static class PointFExtension
    {
        public static float GetDistance(this PointF a, PointF b)
        {
            return MathF.Sqrt((a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y));
        }
    }
}
