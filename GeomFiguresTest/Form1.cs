﻿using GeomFiguresTest.Shapes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeomFiguresTest
{
    public partial class Form1 : Form
    {
        private const int DisplayOffset = 10; // distance between canvas and other elements of GUI

        // list of availabel shapes
        private readonly IReadOnlyList<IShape> _shapes = new List<IShape>
        {
            new CircleShape(),
            new RectangleShape(),
            new SquareShape()
        };

        #region Controls
        // visual editor didn't work for some reason so I decided to create all controls manually
        private readonly PictureBox _canvas; // control used to display shapes
        private readonly Label _help; // control used to display help message
        #endregion

        private int _shapeIndex; // index of selected shape
        private bool _drawing; // if true user started drawing shape
        private readonly PointF[] _points = new PointF[2]; // shape control points

        public Form1()
        {
            InitializeComponent();

            DoubleBuffered = true;

            _help = new Label
            {
                AutoSize = true,
                Text =
@"1. Use mouse wheel to select shape.
2. Move cursor into rectangular area below this text.
3. Press mouse left button.
4. Move cursor in some direction.
5. Enjoy!"
            };

            Controls.Add(_help);

            _canvas = new PictureBox
            {
                BorderStyle = BorderStyle.FixedSingle,
                Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right,
                Left = DisplayOffset,
                Top = _help.DisplayRectangle.Bottom + DisplayOffset,
                Width = ClientSize.Width - DisplayOffset * 2,
                Height = ClientSize.Height - DisplayOffset * 2 - _help.DisplayRectangle.Bottom
            };

            _canvas.MouseDown += _canvas_MouseDown;
            _canvas.MouseMove += _canvas_MouseMove;
            _canvas.MouseUp += _canvas_MouseUp;
            _canvas.Paint += _canvas_Paint;

            Controls.Add(_canvas);

            PrintCurrentShape();

            MouseWheel += Form1_MouseWheel;
        }

        /// <summary>
        /// Handles paint event.
        /// </summary>
        private void _canvas_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(Color.White);

            _shapes[_shapeIndex].Draw(_points, e.Graphics, Brushes.Red);
        }

        /// <summary>
        /// Handles mouse button releasing event.
        /// </summary>
        private void _canvas_MouseUp(object sender, MouseEventArgs e)
        {
            _drawing = false;
            _points[1] = new PointF(e.X, e.Y);
            _canvas.Refresh();
        }

        /// <summary>
        /// Handles mouse movement event.
        /// </summary>
        private void _canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (_drawing)
            {
                _points[1] = new PointF(e.X, e.Y);
                _canvas.Refresh();
            }
        }

        /// <summary>
        /// Handle mouse button pressing event.
        /// </summary>
        private void _canvas_MouseDown(object sender, MouseEventArgs e)
        {
            _drawing = true;
            _points[0] = new PointF(e.X, e.Y);
            _points[1] = new PointF(e.X, e.Y);
            _canvas.Refresh();
        }

        /// <summary>
        /// Handle mouse wheel rotation event.
        /// </summary>
        private void Form1_MouseWheel(object sender, MouseEventArgs e)
        {
            int diff = e.Delta > 0 ? 1 : -1;
            int newIndex = _shapeIndex + diff;

            if (newIndex < 0)
            {
                newIndex = _shapes.Count - 1;
            }
            else if(newIndex >= _shapes.Count)
            {
                newIndex = 0;
            }

            _shapeIndex = newIndex;

            PrintCurrentShape();
        }

        /// <summary>
        /// Adds name of the currently selected shape to the window caption
        /// </summary>
        private void PrintCurrentShape()
        {
            Text = $"Current shape: {_shapes[_shapeIndex].Name}";
        }

    }
}
