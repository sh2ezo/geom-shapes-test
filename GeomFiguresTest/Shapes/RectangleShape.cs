﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace GeomFiguresTest.Shapes
{
    /// <summary>
    /// Draws rectangle.
    /// </summary>
    class RectangleShape : ShapeBase
    {
        /// <inheritdoc />
        public override string Name => "Rectangle";

        /// <inheritdoc />
        public override int ControlPointCount => 2;
        
        /// <inheritdoc />
        protected override void DrawInternal(PointF[] controlPoints, Graphics graphics, Brush brush)
        {
            var center = controlPoints[0];
            var anglePoint = controlPoints[1];
            var sizeX = Math.Abs(center.X - anglePoint.X);
            var sizeY = Math.Abs(center.Y - anglePoint.Y);

            graphics.FillRectangle(brush, center.X - sizeX, center.Y - sizeY, sizeX * 2, sizeY * 2);
        }
    }
}
