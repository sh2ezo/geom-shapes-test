﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace GeomFiguresTest.Shapes
{
    /// <summary>
    /// Interface for drawing shapes.
    /// </summary>
    interface IShape
    {
        /// <summary>
        /// Gets or sets name of the shape.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets or sets number of control points used to describe shape.
        /// </summary>
        int ControlPointCount { get; }

        /// <summary>
        /// Draws shape.
        /// </summary>
        void Draw(PointF[] controlPoints, Graphics graphics, Brush brush);
    }
}
