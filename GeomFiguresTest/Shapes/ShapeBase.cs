﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace GeomFiguresTest.Shapes
{
    /// <summary>
    /// Base class that checks the number of provided points is enough to draw shape.
    /// </summary>
    abstract class ShapeBase : IShape
    {
        /// <inheritdoc />
        public abstract string Name { get; }

        /// <inheritdoc />
        public abstract int ControlPointCount { get; }

        /// <inheritdoc />
        public void Draw(PointF[] controlPoints, Graphics graphics, Brush brush)
        {
            if (controlPoints.Length != ControlPointCount)
            {
                throw new Exception($"Number of provided control points differs from {ControlPointCount}!");
            }

            DrawInternal(controlPoints, graphics, brush);
        }

        /// <summary>
        /// Draws shape. Called after checking the number of provided points is enough to draw the shape.
        /// </summary>
        protected abstract void DrawInternal(PointF[] controlPoints, Graphics graphics, Brush brush);
    }
}
