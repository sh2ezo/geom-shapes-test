﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace GeomFiguresTest.Shapes
{
    /// <summary>
    /// Draws circle.
    /// </summary>
    class CircleShape : ShapeBase
    {
        /// <inheritdoc />
        public override string Name => "Circle";

        /// <inheritdoc />
        public override int ControlPointCount => 2;

        /// <inheritdoc />
        protected override void DrawInternal(PointF[] controlPoints, Graphics graphics, Brush brush)
        {
            var center = controlPoints[0];
            var edgePoint = controlPoints[1];
            var dist = center.GetDistance(edgePoint);

            graphics.FillEllipse(brush, center.X - dist, center.Y - dist, dist * 2, dist * 2);
        }
    }
}
