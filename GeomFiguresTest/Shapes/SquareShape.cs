﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace GeomFiguresTest.Shapes
{
    /// <summary>
    /// Draws square.
    /// </summary>
    class SquareShape : ShapeBase
    {
        /// <inheritdoc />
        public override string Name => "Square";

        /// <inheritdoc />
        public override int ControlPointCount => 2;

        /// <inheritdoc />
        protected override void DrawInternal(PointF[] controlPoints, Graphics graphics, Brush brush)
        {
            var center = controlPoints[0];
            var edgePoint = controlPoints[1];
            var diffX = Math.Abs(center.X - edgePoint.X);
            var diffY = Math.Abs(center.Y - edgePoint.Y);
            var size = Math.Max(diffX, diffY);

            graphics.FillRectangle(brush, center.X - size, center.Y - size, size * 2, size * 2);
        }
    }
}
